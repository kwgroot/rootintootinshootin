﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RootinTootinShootin
{
    class HealthPowerUp : PowerUp
    {
        public HealthPowerUp(int placeNumber) : base("PickupImages/LifeUp", placeNumber)
        {

        }
    }
}