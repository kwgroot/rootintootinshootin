﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RootinTootinShootin
{
    class BulletSpeedPowerUp : PowerUp
    {
        public BulletSpeedPowerUp(int placeNumber) : base("PickupImages/VodkaSpeedUp", placeNumber)
        {

        }
    }
}
