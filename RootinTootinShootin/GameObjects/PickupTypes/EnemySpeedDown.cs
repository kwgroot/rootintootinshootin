﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RootinTootinShootin
{
    class EnemySpeedDown : PowerUp
    {
        public EnemySpeedDown(int placeNumber) : base("PickupImages/SpeedDown", placeNumber)
        {

        }
    }
}
