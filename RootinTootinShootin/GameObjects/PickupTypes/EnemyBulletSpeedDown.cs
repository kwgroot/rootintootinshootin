﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RootinTootinShootin
{
    class EnemyBulletSpeedDown : PowerUp
    {
        public EnemyBulletSpeedDown(int placeNumber) : base("PickupImages/WeaponSpeedDown", placeNumber)
        {

        }
    }
}
